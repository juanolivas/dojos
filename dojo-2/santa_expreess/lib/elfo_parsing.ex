defmodule SantaExpreess.ElfoParsing do
  def read_elfo_script do
    [line] =
    input_path() <> "/input.txt"
      |> File.stream!
      |> Enum.take(1)
    String.graphemes(line)
  end

  defp input_path do
    Application.app_dir(:santa_expreess, "priv")
  end
end