defmodule SantaExpreess.SantaSleigh do

  def move(origin, "v") do
    %SantaExpreess.World{x_value: origin.x_value, y_value: origin.y_value - 1}
  end

  def move(origin, "^") do
    %SantaExpreess.World{x_value: origin.x_value, y_value: origin.y_value + 1}
  end

  def move(origin, "<") do
    %SantaExpreess.World{x_value: origin.x_value - 1, y_value: origin.y_value}
  end

  def move(origin, ">") do
    %SantaExpreess.World{x_value: origin.x_value + 1, y_value: origin.y_value}
  end

end