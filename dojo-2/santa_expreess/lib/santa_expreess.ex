defmodule SantaExpreess do
  @moduledoc """
  Documentation for SantaExpreess.
  """
  alias SantaExpreess.SantaSleigh
  alias SantaExpreess.World
  #v>v<vvv<<vv
  @input ["v",">","v","<","v","v","v","<","<","v","v"]

  def begin_route(instructions) do
    map_set = MapSet.new()
    origin = %World{x_value: 0, y_value: 0}
    start = MapSet.put(map_set, "#{origin.x_value},#{origin.y_value}")
    houses = process_instruction(instructions, origin, start)
    MapSet.size(houses)
  end

  def process_instruction([instruction | tail], coord, map_set) do
    visited_house = SantaSleigh.move(coord, instruction)
    updated_houses = MapSet.put(map_set, "#{visited_house.x_value},#{visited_house.y_value}")
    process_instruction(tail, visited_house, updated_houses)
  end

  def process_instruction([], _, result) do
    result
  end


end
