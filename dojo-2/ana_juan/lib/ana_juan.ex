defmodule AnaJuan do
  def deliver_gifts() do
    input =
      Path.expand("../input.txt", __DIR__)
      |> File.read!()
      |> String.split("", trim: true)

    map = %{x: 0, y: 0}
    places = MapSet.new()

    run_instructions(map, input, places)
  end

  defp run_instructions(map, [tail | head], places) do
    parameter = tail

    places =
      places
      |> MapSet.put(map)

    new =
      map
      |> set_cord(parameter)

    run_instructions(new, head, places)
  end

  defp run_instructions(_map, list, places) when list == [] do
    "Esta navidad Santa visito: #{MapSet.size(places)} casas"
  end

  defp set_cord(%{x: x, y: y}, "<") do
    %{x: x - 1, y: y}
  end

  defp set_cord(%{x: x, y: y}, ">") do
    %{x: x + 1, y: y}
  end

  defp set_cord(%{x: x, y: y}, "v") do
    %{x: x, y: y - 1}
  end

  defp set_cord(%{x: x, y: y}, "^") do
    %{x: x, y: y + 1}
  end
end
