defmodule RobotSanta do
  def deliver_gifts() do
    input =
      Path.expand("../input.txt", __DIR__)
      |> File.read!()
      |> String.split("", trim: true)
    map = %{x: 0, y: 0}
    places = MapSet.new()

    robo_result =
    Enum.drop_every(input, 2)
    |> run_instructions(map, places)

    santa_result =
    Enum.take_every(input, 2)
    |> run_instructions(map, places)

    "Esta navidad Santa y RoboSanta visitoron: #{robo_result + santa_result} casas"

  end

  def run_instructions([tail | head], map, places) do
    parameter = tail

    places =
      places
      |> MapSet.put(map)

    new =
      map
      |> set_cord(parameter)

    run_instructions(head, new, places)
  end

  def run_instructions(list, _map, places) when list == [] do
    MapSet.size(places)
  end

  defp set_cord(%{x: x, y: y}, "<") do
    %{x: x - 1, y: y}
  end

  defp set_cord(%{x: x, y: y}, ">") do
    %{x: x + 1, y: y}
  end

  defp set_cord(%{x: x, y: y}, "v") do
    %{x: x, y: y - 1}
  end

  defp set_cord(%{x: x, y: y}, "^") do
    %{x: x, y: y + 1}
  end
end
