defmodule AnaJuanTest do
  use ExUnit.Case
  doctest AnaJuan


  test "just_santa" do
    result = AnaJuan.deliver_gifts
    expected = "Esta navidad Santa visito: 2572 casas"
    assert result == expected
  end

  test "new_system" do
    result = RobotSanta.deliver_gifts
    expected = "Esta navidad Santa y RoboSanta visitoron: 2784 casas"
    assert result == expected
  end
end
