defmodule Dojov1.Commands do
  require Integer
  def execute(steps, registers, line \\ 0)
  
  def execute(steps, registers, line) do
    if line >= length(steps) do
      execute(steps, registers, line, :end)
    else
      step = 
      Enum.at(steps, line)
      |> String.split()

      instruction =
        case step do
          [] -> false
          _ -> Enum.at(step, 0)
        end

      register = 
        case step do
          [] -> nil
          step -> 
            Enum.at(step, 1)
            |> String.replace(",", "")
        end
      lines = Enum.at(step, 2)
  
      case instruction do
        "hlf" -> hlf(steps, register, registers, line)
        "tpl" -> tpl(steps, register, registers, line)
        "inc" -> inc(steps, register, registers, line)
        "jmp" -> jmp(steps, register, registers, line)
        "jie" -> jie(steps, register, registers, line, lines)
        "jio" -> jio(steps, register, registers, line, lines)   
        _ -> registers
      end
    end

  end

  def execute(_steps, registers, _line, :end) do
    registers
  end

  def hlf(steps, register, registers, line) do
    registers = Map.update(registers, register, 0, &div(&1, 2))
    execute(steps, registers, line + 1)
  end

  def tpl(steps, register, registers, line) do
    registers = Map.update(registers, register, 0, &(&1 * 3))
    execute(steps, registers, line + 1)
  end

  def inc(steps, register, registers, line) do
    registers = Map.update(registers, register, 0, &(&1 + 1))
    execute(steps, registers, line + 1)
  end

  def jmp(steps, register, registers, line) do
    {number, _} =
      register
      |> Integer.parse()

    execute(steps, registers, line + number)
  end

  def jmp(steps, _register, registers, line, lines) do
    execute(steps, registers, line + lines)
  end

  def jie(steps, register, registers, line, lines) do
    {number, _} =
      lines
      |> Integer.parse()

    value = Map.get(registers, register)

    if Integer.is_even(value) do
      jmp(steps, register, registers, line, number)
    else
      execute(steps, registers, line + 1)
    end
  end

  def jio(steps, register, registers, line, lines) do
    {number, _} =
      lines
      |> Integer.parse()

      value = Map.get(registers, register)

    if value == 1 do
      jmp(steps, register, registers, line, number)
    else 
      execute(steps, registers, line + 1)
    end
  end
end