defmodule Dojov1 do
  @moduledoc """
  Documentation for Dojov1.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Dojov1.hello
      :world

  """
  def run() do
    steps =
      Path.expand("../input.txt", __DIR__)
      |> File.read!()
      |> String.split("\n")
      |> Enum.map(&String.trim/1)

    Dojov1.Commands.execute(steps, %{"a" => 0, "b" => 0})
  end
end
