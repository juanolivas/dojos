defmodule Bunsan.Instructions do
  require Integer

  def hlf(register) do
    value = register.value
    result = value / 2
    %{register | value: result}
  end

  def tpl(register) do
    value = register.value
    result = value * 3
    %{register | value: result}
  end

  def inc(register) do
    value = register.value
    result = value + 1
    %{register | value: result}
  end

  def jmp(offset, stack) do
    offset
  end

  def jie(register, offset, stack) do
    if Integer.is_even(register.value) do
      jmp(offset, stack)
    end
  end

  def jio(register, offset, stack) do
    if register.value == 1 do
      jmp(offset, stack)
    end
  end

end