defmodule Bunsan.Mnemonic do
  defstruct [:instruction, :param1, :param2]
end