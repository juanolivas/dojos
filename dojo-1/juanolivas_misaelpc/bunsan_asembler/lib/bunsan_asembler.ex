defmodule BunsanAsembler do
  @moduledoc """
  Documentation for BunsanAsembler.
  """

  @doc """
  Hello world.

  ## Examples

      iex> BunsanAsembler.hello()
      :world

  """
  def run(program) do
    register_a = %Bunsan.Register{value: 0}
    register_b = %Bunsan.Register{value: 0}
    mnemonics = Enum.map(program, fn mnemonic ->
      parse_mnemonic(mnemonic)
    end)
    {:ok, pid} = Agent.start(fn -> %{"a" => register_a, "b" => register_b, "stack" => mnemonics} end)

    IO.inspect iterate_stack(mnemonics, pid, 0)
    Agent.get(pid, fn state -> state end)
  end

  def iterate_stack([head|tail], pid_agent, index) do
    mnemonic = head
    if mnemonic.param2 == nil do
      case mnemonic.param1 do
        "jmp" ->
          stack = Agent.get(pid_agent, fn state -> Map.fetch!(state, "stack") end)
          apply(Bunsan.Instructions, String.to_atom(mnemonic.instruction), [mnemonic.param2, stack, index])
          iterate_stack(tail, pid_agent, index + 1)
        _ ->
          Agent.get_and_update(pid_agent, fn state ->
            register = Map.fetch!(state, mnemonic.param1)
            update_register = apply(Bunsan.Instructions, String.to_atom(mnemonic.instruction), [register])
            {state, %{state | mnemonic.param1 => update_register}}
          end)
          iterate_stack(tail, pid_agent, index + 1)
      end
    else
      register = Agent.get(pid_agent, fn state -> Map.fetch!(state, mnemonic.param1) end)
      stack = Agent.get(pid_agent, fn state -> Map.fetch!(state, "stack") end)
      apply(Bunsan.Instructions, String.to_atom(mnemonic.instruction), [register, mnemonic.param2, stack, index])
      iterate_stack(tail, pid_agent, index + 1)
    end
  end

  def iterate_stack([], _, index), do: index

  def parse_mnemonic(mnemonic) do
    case String.split(mnemonic, ",") do
      [element1, param2] ->
        [instruction, param1] = String.split(element1, " ")
        %Bunsan.Mnemonic{instruction: instruction,
                              param1: param1,
                              param2: param2}
      [element1] ->
        [instruction, param1] = String.split(element1, " ")
        %Bunsan.Mnemonic{instruction: instruction,
                              param1: param1}
    end
  end
end
