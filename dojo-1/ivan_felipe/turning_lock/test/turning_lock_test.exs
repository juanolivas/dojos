defmodule TurningLockTest do
  use ExUnit.Case
  doctest TurningLock

  test "should increment the value of a register" do
    registers = TurningLock.execute_instruction("inc a", %{"a" => 0, "b" => 0})

    assert registers["a"] == 1
    assert registers["b"] == 0

    registers = TurningLock.execute_instruction("inc a", registers)

    assert registers["a"] == 2
    assert registers["b"] == 0

    registers = TurningLock.execute_instruction("inc b", registers)

    assert registers["a"] == 2
    assert registers["b"] == 1
  end

  test "should triple the value of a register" do
    registers = TurningLock.execute_instruction("tpl a", %{"a" => 0, "b" => 0})
    assert registers["a"] == 0
    assert registers["b"] == 0

    registers = TurningLock.execute_instruction("inc a", registers)
    registers = TurningLock.execute_instruction("tpl a", registers)

    assert registers["a"] == 3
    assert registers["b"] == 0
  end

  test "should half the value of a register" do
    registers = TurningLock.execute_instruction("hlf a", %{"a" => 0, "b" => 0})
    assert registers["a"] == 0
    assert registers["b"] == 0

    registers = TurningLock.execute_instruction("inc a", registers)
    registers = TurningLock.execute_instruction("inc a", registers)
    registers = TurningLock.execute_instruction("inc a", registers)
    registers = TurningLock.execute_instruction("tpl a", registers)
    registers = TurningLock.execute_instruction("hlf a", registers)

    assert registers["a"] == 4
    assert registers["b"] == 0
  end

  test "should jump n number of instructions" do
    instructions = ["inc a", "jmp 1"]
    registers = TurningLock.execute_instructions(instructions, %{"a" => 0, "b" => 0})
    assert registers["a"] == 1
    assert registers["b"] == 0

    instructions = ["inc a", "jmp 2", "tpl a", "inc a"]
    registers = TurningLock.execute_instructions(instructions, %{"a" => 0, "b" => 0})
    assert registers["a"] == 2
    assert registers["b"] == 0
  end

  test "should jump n number of instructions only if the value is even" do
    instructions = ["inc a", "tpl a", "jie a, 2", "tpl a", "inc a"]
    registers = TurningLock.execute_instructions(instructions, %{"a" => 0, "b" => 0})
    assert registers["a"] == 10
    assert registers["b"] == 0
  end

  test "should jump n number of instructions only if the value is one" do
    instructions = ["inc a", "jio a, +2", "tpl a", "inc a"]
    registers = TurningLock.execute_instructions(instructions, %{"a" => 0, "b" => 0})
    assert registers["a"] == 2
    assert registers["b"] == 0
  end
end
