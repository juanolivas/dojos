defmodule TurningLock do
  @moduledoc """
  Documentation for TurningLock.
  """

  def run() do
    instructions =
      Path.expand("../input.txt", __DIR__)
      |> File.read!()
      |> String.split("\n")
      |> Enum.map(&String.trim/1)

    TurningLock.execute_instructions(instructions, %{"a" => 0, "b" => 0})
  end

  def execute_instructions(instructions, registers, line \\ 0) do
    instruction = Enum.at(instructions, line)

    case execute_instruction(instruction, registers) do
      %{} = registers ->
        execute_instructions(instructions, registers, line + 1)

      {:jump, lines} ->
        execute_instructions(instructions, registers, line + lines)

      {:end, registers} ->
        registers
    end
  end

  def execute_instruction("inc " <> register, registers) do
    Map.update(registers, register, 0, &(&1 + 1))
  end

  def execute_instruction("tpl " <> register, registers) do
    Map.update(registers, register, 0, &(&1 * 3))
  end

  def execute_instruction("hlf " <> register, registers) do
    Map.update(registers, register, 0, &div(&1, 2))
  end

  def execute_instruction(nil, registers), do: {:end, registers}

  def execute_instruction("", registers), do: {:end, registers}

  def execute_instruction("jmp " <> lines, _registers) do
    lines
    |> parse_number()
    |> validate_jump(true, fn value -> value end)
  end

  def execute_instruction("jie " <> value, registers) do
    value
    |> String.split(",")
    |> Enum.map(&String.trim/1)
    |> jump_lines(registers, fn value -> rem(value, 2) == 0 end)
  end

  def execute_instruction("jio " <> value, registers) do
    value
    |> String.split(",")
    |> Enum.map(&String.trim/1)
    |> jump_lines(registers, fn value -> value == 1 end)
  end

  defp jump_lines([register, lines], registers, func) do
    value = Map.get(registers, register)

    lines
    |> parse_number()
    |> validate_jump(value, func)
  end

  defp validate_jump(lines, value, func) when is_number(lines) do
    if func.(value), do: {:jump, lines}, else: {:jump, 1}
  end

  defp parse_number(string) do
    case Integer.parse(string) do
      {number, _res} -> number
      _ -> :error
    end
  end
end
